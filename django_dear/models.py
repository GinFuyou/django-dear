import logging

import magic
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.files.storage import get_storage_class
from django.db import models
from django.utils import timezone
from django.utils.deconstruct import deconstructible
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger('django')

UPLOAD_TO = getattr(settings, 'DEAR_UPLOAD_TO', 'records/')
STORAGE = getattr(settings, 'DEAR_RECORD_STORAGE', None)
DEFALT_STORAGE = 'django.core.files.storage.FileSystemStorage'

# warn user if DEFAULT_FILE_STORAGE is changed, but DEAR_RECORD_STORAGE is not.
if not STORAGE:
    if settings.DEFAULT_FILE_STORAGE == DEFALT_STORAGE:
        logger.warn(_("Your settings.DEFAULT_FILE_STORAGE is changed from"
                      " default value, but DEAR_RECORD_STORAGE is not set and"
                      " will keep using FileSystemStorage subclass. If it's"
                      " desired, set DEAR_RECORD_STORAGE to remove this warning."))
    STORAGE = 'django_dear.storage.RecordStorage'
RecordStorage = get_storage_class(STORAGE)


@deconstructible
class RecordValidator:
    """ Based on https://github.com/mckinseyacademy/django-upload-validator/ """
    mime_message = _("MIME type '%(mimetype)s' is not allowed. Please check your model validators.")
    max_size_message = _('The current record size is %(size)s. The maximum file size is %(max_size)s.')

    def __init__(self,
                 allowed_types=("audio/ogg", ),
                 read_size=2048 * 1024,
                 max_size=getattr(settings, 'DEAR_MAX_SIZE', 2048 * 1024)):
        #
        self.allowed_types = allowed_types
        self.read_size = read_size
        self.max_size = max_size

    def __call__(self, fileobj):
        # check file size if it's set
        if self.max_size:
            filesize = len(fileobj)
            if filesize > self.max_size:
                msg = self.max_size_message % {'size': filesize, 'max_size': self.max_size}
                raise ValidationError(msg)

        # check MIME type with python-magic
        detected_type = magic.from_buffer(fileobj.read(self.read_size), mime=True)
        print(f"* detected_type = {detected_type}")
        if detected_type not in self.allowed_types:
            msg = self.mime_message % {'mimetype': detected_type}
            raise ValidationError(msg)
        return


class AbstractBaseRecord(models.Model):
    """ most basic record model only has creation datetime and record file """
    class Meta:
        abstract = True

    file = models.FileField(_('record file'),
                            upload_to=UPLOAD_TO,
                            validators=[RecordValidator()],
                            storage=RecordStorage())

    create_datetime = models.DateTimeField(
        verbose_name=_('date and time created'),
        default=timezone.now
    )


# TODO: add file cleanup
class AbstractRecord(AbstractBaseRecord):
    class Meta:
        abstract = True
        indexes = [
            models.Index(fields=['uuid']),
        ]

    uuid = models.UUIDField(unique=True)
    uuid.help_text = _('uuid expected from them frontend to replace recording file')

    is_used = models.BooleanField(_('is used'), default=False)
    is_used.help_text = _('if record was marked as used, or just temporary stored')

    def __str__(self):
        return self.file.name


class AudioRecord(AbstractRecord):
    """ concrete audio record model """
    pass
