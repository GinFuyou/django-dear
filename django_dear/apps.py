from django.apps import AppConfig


class DjangoDearConfig(AppConfig):
    name = 'django_dear'
