import logging
from pathlib import Path

from django.contrib import messages
from django.core.files.storage import FileSystemStorage

logger = logging.getLogger('django')


class RecordStorage(FileSystemStorage):
    """ FileSystemStorage that overwrites files silently """
    backup_suffix = ".bckp"
    restore_fail_raises = False

    def get_available_name(self, name, max_length=None):
        """ Always returns name as available, but if it existed, rename existing file.
            Backup is removed or restored on save()
        """

        if self.exists(name):
            abs_path = self.path(name)
            Path(abs_path).rename(abs_path + self.backup_suffix)
        return name

    def save(self, name, content, **kwargs):
        try:
            filename = super().save(name, content, **kwargs)
        except Exception as save_exc:  # catch generic exception, will raise it later
            backup_name = name + self.backup_suffix
            # try to restore backup if it exists
            if self.exists(backup_name):
                try:
                    Path(self.path(backup_name)).rename(self.path(name))
                except Exception as restore_exc:
                    logger.warn(
                        f"{self.__class__.__name__} couldn't restrore"
                        f" {backup_name} on failed save: {restore_exc}!"
                    )
                    if self.restore_fail_raises:
                        raise restore_exc

            raise save_exc  # raise a generic exception, we don't want to hide it.
        else:
            backup_name = filename + self.backup_suffix
            print(f"backup: {backup_name}")
            if self.exists(backup_name):
                print(f'remove {backup_name}')
                self.delete(backup_name)

        return filename
