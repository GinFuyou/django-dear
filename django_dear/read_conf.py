from pathlib import Path

import toml

PARENT_DIR = Path(__file__).resolve().parent


def read_toml(*path_parts, base_dir=PARENT_DIR, just_poetry=False):
    if not isinstance(base_dir, Path):
        base_dir = Path(base_dir).resolve()
    toml_path = base_dir / Path(*path_parts)
    config_dict = toml.load(toml_path)
    if just_poetry:
        return config_dict['tool']['poetry']
    else:
        return config_dict
