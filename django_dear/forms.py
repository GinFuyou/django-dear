import json
from pprint import pprint
from uuid import uuid4

from django import forms
from django.apps import apps
from django.conf import settings
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext as _

from . import __version__ as dear_version
from .models import AudioRecord, RecordValidator

DEAR_HANDLER_URL = reverse_lazy(getattr(settings, 'DEAR_HANDLER_URL', 'dear:save_record'))
DEAR_DATA_SRC_SELECTOR = getattr(settings, 'DEAR_DATA_SRC_SELECTOR', '_dear-data')
JSON_DATA_ATTR = 'data-dear-json'


def get_config_attributes(form=None, **kwargs):
    if form:
        form_id_prefixing = form.auto_id % form.prefix
    else:
        form_id_prefixing = "id_dear"

    attrs = {'output_id': '',
             'form_action': str(DEAR_HANDLER_URL),
             'version': dear_version,  # NOTE remove later?
             # NOTE important - that's how script knows the input
             'class': DEAR_DATA_SRC_SELECTOR,
             'form_id_prefixing': form_id_prefixing,
             'token_input': 'uuid'  # without prefix
             }

    attrs.update(kwargs)
    if form:
        attrs['dear_prefix'] = form.output_id
    else:
        attrs['dear_prefix'] = attrs['output_id']

    return attrs


class RecordContextMixin:
    class Media:
        js = ("dear-js/dear-form.js", )

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)

        # for RecordWidget output_id is set by form, ComboRecordWidget already has it
        output_id = context['widget']['attrs'].pop('output_id', None)
        if output_id:
            context['output_id'] = output_id

        context.update(
            {
                'buttons':
                {
                    'start': {'label': _('Start recording'), 'attrs': {}},
                    'stop': {'label': _('Stop'), 'attrs': {}},
                },
                'file_link': self.file_link,
            }
        )
        return context


class RecordWidget(RecordContextMixin, forms.FileInput):
    input_type = 'hidden'
    needs_multipart_form = False
    template_name = 'dear_widgets/record_widget.html'
    file_link = {'attrs': {'target': "_blank"}}  # exactly None to remove


class ComboRecordWidget(RecordContextMixin, forms.TextInput):
    template_name = 'dear_widgets/combo_widget.html'
    file_link = {'attrs': {'target': "_blank"}}  # exactly None to remove

    def __init__(self, attrs=None, choices=(), token_callable=uuid4, data_src=DEAR_DATA_SRC_SELECTOR):
        super().__init__(attrs)
        self.choices = list(choices)
        self.token_callable = token_callable
        self.data_src = data_src

    def get_context(self, name, value, attrs):
        data = get_config_attributes(output_id=attrs.get('id'))
        dear_attrs = {'required': False, 'class': data['class']}
        dear_attrs[JSON_DATA_ATTR] = json.dumps(data)
        context = super().get_context(name=self.data_src, value='---', attrs=dear_attrs)
        context['widget'].update(
            {'form_token': str(self.token_callable()),
             'required': False,
             'is_hidden': True,
             'token_input_id': f"{data['form_id_prefixing']}-{data['token_input']}"}
        )
        context['output_id'] = attrs.get('id')
        if value:
            related_context = forms.Select(self.attrs, choices=self.choices).get_context(name, value, attrs)
            context['related_widget'] = ['widget']
        else:
            self.attrs['placeholder'] = _("Start recording!")
            related_context = forms.TextInput(self.attrs).get_context(name, value, attrs)
        context['related_widget'] = related_context['widget']

        context['widget'].pop('type')
        context['widget'].pop('value')
        pprint(context)  # WARNING TEST
        return context


class RecordBlobField(forms.FileField):
    widget = RecordWidget(attrs={'hidden': True})

    def to_python(self, data):
        print(f'to python: {data}')
        return super().to_python(data)


class AudioRecordForm(forms.ModelForm):
    form_id_field = 'uuid'
    handler_pattern_name = 'dear:save_record'
    main_field = 'record'

    class Meta:
        model = AudioRecord
        exclude = ['create_datetime', 'is_used', 'file']

    # don't forget validator here, because main file field of model is excluded!
    record = RecordBlobField(validators=[RecordValidator()])

    def __init__(self, *args, data=None, files=None,
                 instance=None, output_field_id=None, **kwargs):

        self.output_id = output_field_id

        initial = kwargs.get('initial', {})
        if not initial.get(self.form_id_field):
            initial[self.form_id_field] = uuid4()
            kwargs['initial'] = initial  # that's should be excessive

        # make for into 'create or update' one, doing instance retrieve when there is data
        if instance is None and data:
            model_cls = self._meta.model
            try:
                instance = model_cls._default_manager.get(
                    **{self.form_id_field: data.get(self.form_id_field)}
                )
                print(f're-using instance #{instance.pk} ({data.get(self.form_id_field)})')
            except model_cls.DoesNotExist:
                instance = model_cls()
        super().__init__(data, files, *args, instance=instance, **kwargs)

        data = get_config_attributes(self, output_id=self.output_id)
        self.fields[self.main_field].widget.attrs.update(
            {JSON_DATA_ATTR: json.dumps(data),
             'class': data['class'],
             'output_id': self.output_id}
        )
        self.fields[self.main_field].widget.output_id = self.output_id

    # def clean(self):
        # data = self.cleaned_data
        # print(f'cleaned data: {data}')
        # print(f"files: {self.files}")

    def save(self, commit=True):
        self.instance.file = self.cleaned_data[self.main_field]
        return super().save(commit=commit)
