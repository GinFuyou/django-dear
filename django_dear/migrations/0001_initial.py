# Generated by Django 3.1.5 on 2021-01-05 09:19
# flake8: noqa: E501

import django.utils.timezone
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AudioRecord',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file', models.FileField(upload_to='', verbose_name='record file')),
                ('create_datetime', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date and time created')),
                ('uuid', models.UUIDField(help_text='uuid expected from them frontend to replace recording file', unique=True)),
                ('is_used', models.BooleanField(default=False, help_text='if record was marked as used, or just temporary stored', verbose_name='is used')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddIndex(
            model_name='audiorecord',
            index=models.Index(fields=['uuid'], name='django_dear_uuid_abf811_idx'),
        ),
    ]
