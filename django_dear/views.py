# Create your views here.
import logging

from django import forms
from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView

from .forms import AudioRecordForm

logger = logging.getLogger('django')


class RecordSaveView(CreateView):
    template_name = "easy_records/audio.html"
    form_class = AudioRecordForm

    # def post(self, request, *args, **kwargs):
    #    print(f"POST: {self.request.POST}\nFILES: {self.request.FILES}")
    #    return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        self.object = form.save()
        data = {'status': "OK",
                "url": self.object.file.url,
                "size": self.object.file.size,
                "object_id": self.object.pk}
        # if self.related_input_attrs:
        #    attrs = self.related_input_attrs
        #    data['input_id'] = attrs['id']
        return JsonResponse(data)

    def form_invalid(self, form):
        logger.warning(f'Dear form invalid: {form.errors.as_json()}')
        return JsonResponse(
            {'status': "Error",
             "errors": form.errors.as_json()}
        )
