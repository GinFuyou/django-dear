from django.urls import include, path

from .views import RecordSaveView

app_name = 'dear'
urlpatterns = [
    path('save-record/', RecordSaveView.as_view(), name='save_record'),
]
