from django.contrib import admin

# Register your models here.
from .models import AudioRecord


@admin.register(AudioRecord)
class AudioRecordAdmin(admin.ModelAdmin):
    list_display = ('pk', 'uuid', 'file', 'create_datetime')
