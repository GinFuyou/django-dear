var _deaRecordButton, _dearStopButton, dearecorder, _dearMainField;
var _dearMainFieldClass = "_dear-data"; // WARNING HARDCODED
var _dearData;

window.onload = function () {
    // change this line to make different selection behaviour of main field input
    _dearMainField = document.getElementsByClassName(_dearMainFieldClass)[0]
    if (!(_dearMainField))
    {
        console.log("Warning! couldn't load _dearMainField by selector '" + _dearMainFieldClass + "'")
    };
    _dearData = JSON.parse(_dearMainField.getAttribute('data-dear-json'));
    console.log(_dearData);

    console.log("dear prefix: "+_dearData.dear_prefix);
    _dearRecordButton = document.getElementById(_dearData.dear_prefix+'-start');
    _dearStopButton = document.getElementById(_dearData.dear_prefix+'-stop');

  // get audio stream from user's mic
  navigator.mediaDevices.getUserMedia({
    audio: true
  })
  .then(function (stream) {
    _dearRecordButton.disabled = false;
    _dearRecordButton.addEventListener('click', startRecording);
    _dearStopButton.addEventListener('click', stopRecording);
    dearecorder = new MediaRecorder(stream);

    // listen to dataavailable, which gets triggered whenever we have
    // an audio blob available
    dearecorder.addEventListener('dataavailable', onRecordingReady);
  });
};


function startRecording(e) {
  e.preventDefault();
  _dearRecordButton.disabled = true;
  _dearStopButton.disabled = false;

  dearecorder.start();
}

function stopRecording(e) {
  e.preventDefault();
  _dearRecordButton.disabled = false;
  _dearStopButton.disabled = true;

  // Stopping the dearecorder will eventually trigger the `dataavailable` event and we can complete the recording process
  dearecorder.stop();
}

function recordSaved(data)
{
    console.log("recordSaved:");
    console.log(data);
    if (data.status == 'OK')
    {
        console.log(_dearData.dear_prefix+'-link');
        var link = document.getElementById(_dearData.dear_prefix+'-link');
        link.setAttribute('href', data.url);
        link.innerHTML = "Record #" + data.object_id + " (" + data.size + " b)";
        if (_dearData.output_id)
        {
            var out_field = document.getElementById(_dearData.output_id);
            if (!(out_field))
            {
                out_field = document.createElement("input");
                out_field.setAttribute('id', _dearData.output_id);
                document.append(out_field);
            }
            out_field.disabled = false;
            out_field.setAttribute('value', data.object_id);
        }
    }
    else
    {
        if (data.errors)
        {
            /*
            console.log(JSON.parse(data.errors));
            for(var i=0; i++; data.errors.length)
            {
                console.log("> "+data.errors[i]);
            };
            */
            alert("Couldn't save record: " + data.errors);
        }
    }
}

function onRecordingReady(e) {
    var audio = document.getElementById(_dearData.dear_prefix+"-audio");
    // e.data contains a blob representing the recording
    audio.src = URL.createObjectURL(e.data);
    audio.play();
    console.log('size: '+e.data.size);

    console.log("Output to: #" + _dearData.output_id)
    var fd = new FormData();
    var uuid_id = _dearData.form_id_prefixing + '-' + _dearData.token_input;
    console.log("uuid ID: " + uuid_id);
    var uuid = document.getElementById(uuid_id).value;
    fd.append('uuid', uuid);
    fd.append('csrfmiddlewaretoken', document.getElementsByName('csrfmiddlewaretoken')[0].value);

    // TODO make field name dynamic
    fd.append('record', e.data, 'record-'+uuid+'.ogg');
    console.log('submit to url: ' +  _dearData.form_action);

    submitRecord(_dearData.form_action, fd)
        .then(data => recordSaved(data))
        .catch(error => function on_error(error){
            console.error(error);
            alert(error);
        });
}


async function submitRecord(url, formData) {
    const response = await fetch(url,
    {
        method: 'POST',
        body: formData
    });

    if (!response.ok)
    {
        var msg = "Error at ${url}, status was: ${response.status}";
        alert(msg);
        throw new Error(msg);
    }
    return await response.json();
}
