# -*- coding: utf-8 -*-
from setuptools import setup

packages = ['django_dear',
            'django_dear.migrations']

package_data = {'': ['*'], 'django_dear': ['templates/dear_widgets/*']}

install_requires = ['python-magic']

setup_kwargs = {
    'name': 'django-dear',
    'version': '0.15.1a',
    'description': 'Easy Audio Recording integration for django forms',
    'long_description': None,
    'author': 'Valentine Davydov',
    'author_email': '42@doratoa.net',
    'maintainer': None,
    'maintainer_email': None,
    'url': None,
    'packages': packages,
    'package_data': package_data,
    'install_requires': install_requires,
    'python_requires': '>=3.6.4,<4.0.0',
}


setup(**setup_kwargs)
