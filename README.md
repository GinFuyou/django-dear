# Django Easy Audio Recording
> WARNING ! Work in progress, not ready for production use! Feel free to fork and fix yourself.

## Usage (alpha)

### AudioRecordForm

 - install with pip or poetry
 - add `django_dear` into your `INSTALLED_APPS`
 - add handler to your urls.py `path("records/", include("django_dear.urls"))`
 - add ForeignKey to a model, that will hold reference to record object:
 ```python
 class ObjectWithRecord(models.Model):
    # other fields
    record_entry = models.ForeignKey("django_dear.AudioRecord", on_delete=models.CASCADE)
```

 - in the view that handles you form, that needs ataching a record, import `from django_dear.forms import AudioRecordForm`, add it's instance to your context (doesn't have to handle POST, submit is done with AJAX to separate view). Add `output_field_id` kwarg which is id of your *main* form field that has Foreign Key to AudioRecord model.
```python
context['record_form'] = AudioRecordForm(output_field_id="id_record_entry", prefix="dear")
```
 - create migrations and migrate
 - inject `record_form` into your main form tag. csrftoken can be read from it too.
 - Start recording. File is submitted automatically when record stopped. Starting record again swaps the file (behaviours supposed to extended)

### ComboRecordWidget

`django_dear.forms.ComboRecordWidget` is *almost* self-sufficient widget implementing recording.
Right now it lack proper handling of 'update' mode and and on creating only outputs raw record PK

```python
from django import forms
from django_dear.forms import ComboRecordWidget


class RecordForm(forms.ModelForm):
    class Meta:
        model = ObjectWithRecord
        fields = ['title', 'record_entry']
        widgets = {'record_entry': ComboRecordWidget()}


@admin.register(ObjectWithRecord)
class ObjectWithRecordAdmin(admin.ModelAdmin):
    form_class = RecordForm
    list_display = ('title', 'record_entry')

    def get_form(self, request, obj=None, **kwargs):
        kwargs['form'] = RecordForm
        return super().get_form(request, obj, **kwargs)
```

## TODO

 - Improve javascript to better handle `ComboRecordWidget` workflows
 - Add record validation (you can submit really anything now, not good)
 - Consider adding a form Field subclass in addition to widgets
 - Remove jQuery
